const questions = [
	{
		question: "Какой природный ресурс используется на ВЭС для производства электроэнергии?",
		answers: [
			{ text:"Поток воздуха и энергия ветра", correct: true},
			{ text:"Солнечная радиация", correct: false},
			{ text:"Потенциал воды", correct: false},
			{ text:"Геотермальная энергия", correct: false},
			
			]
	},

	{
		question: "В какой стране находится ГЭС Трех Ущелий, являющаяся одним из крупнейших гидроэлектростанций в мире?",
		answers: [
			{ text:"Россия", correct: false},
			{ text:"Китай", correct: true},
			{ text:"Бразилия", correct: false},
			{ text:"США", correct: false},
			
			]
	},

		{
		question: "Каково основное назначение АЭС (атомной электростанции)?",
		answers: [
			{ text:"Использование энергии ветра для производства электричества", correct: false},
			{ text:"Генерация электроэнергии из солнечной радиации", correct: false},
			{ text:"Производство электроэнергии из ядерного топлива", correct: true},
			{ text:"Использование потенциала воды для преобразования в электричество", correct: false},
			
			]
	},

		{
		question: "Какое государство является лидером по производству электроэнергии из возобновляемых источников?",
		answers: [
			{ text:"Германия", correct: false},
			{ text:"Россия", correct: false},
			{ text:"Китай", correct: true},
			{ text:"США", correct: false},
			
			]
	},

		{
		question: "Какой вид энергии является основным источником электричества в России?",
		answers: [
			{ text:"Атомная", correct: false},
			{ text:"Солнечная", correct: false},
			{ text:"Гидроэнергия", correct: false},
			{ text:"Тепловая", correct: true},
			
			]
	},

		{
		question: "В каком регионе России находится самая большая электростанция по мощности?",
		answers: [
			{ text:"Республика Хакаси", correct: true},
			{ text:"Республика Коми", correct: false},
			{ text:"Республика Саха", correct: false},
			{ text:"Московская область", correct: false},
			
			]
	},

		{
		question: "Какая электростанция является крупнейшей солнечной станцией в России?",
		answers: [
			{ text:"Старомарьевская СЭС", correct: false},
			{ text:"Аршанская СЭС", correct: true},
			{ text:"Самарская СЭС", correct: false},
			{ text:"Фунтовская СЭС", correct: false},
			
			]
	},

		{
		question: "Назовите крупнейшую ветрогенераторную установку в России.",
		answers: [
			{ text:"Кочубеевская ВЭС", correct: false},
			{ text:"Адыгейская ВЭС", correct: false},
			{ text:"Ульяновская ВЭС", correct: true},
			{ text:"Кольская ВЭС", correct: false},
			
			]
	},

		{
		question: "Какая электростанция является крупнейшей тепловой станцией в России?",
		answers: [
			{ text:"Сургутская ГРЭС-1", correct: false},
			{ text:"Костромская ГРЭС", correct: false},
			{ text:"Рефтинская ГРЭС", correct: false},
			{ text:"Сургутская ГРЭС-2", correct: true},
			
			]
	},

		{
		question: "Сколько процентов электроэнергии в России производят ВЭС?",
		answers: [
			{ text:"0.4%", correct: true},
			{ text:"0.7%", correct: false},
			{ text:"2%", correct: false},
			{ text:"0.9%", correct: false},
			
			]
	}
];

const questionElement = document.getElementById("question");
const answerButtons = document.getElementById("answer-buttons");
const nextButton = document.getElementById("next-bttn");

let currentQuestionIndex = 0;
let score = 0;

function startQuiz(){
	currentQuestionIndex = 0;
	score = 0;
	nextButton.innerHTML = "Далее";
	showQuestion();
}

function showQuestion(){
	resetState();
	let currentQuestion = questions[currentQuestionIndex];
	let questionNo = currentQuestionIndex + 1;
	questionElement.innerHTML = questionNo + "." + currentQuestion.question;

	currentQuestion.answers.forEach(answer => {
		const button = document.createElement("button");
		button.innerHTML = answer.text;
		button.classList.add("bttn");
		answerButtons.appendChild(button);
	 if(answer.correct){
		button.dataset.correct = answer.correct;
	}
	button.addEventListener("click", selectAnswer);
	});
}

function resetState(){
	nextButton.style.display = "none";
	while(answerButtons.firstChild){
		answerButtons.removeChild(answerButtons.firstChild);
	}
}

function selectAnswer(e){
	const selectedBtn = e.target;
	const isCorrect = selectedBtn.dataset.corrent === "true";
	if(isCorrect){
		selectedBtn.classList.add("correct");
		score++;
	}else{
		selectedBtn.classList.add("incorrect");
	}
	Array.from(answerButtons.children).forEach(button => {
		if(button.dataset.correct === "true"){
			button.classList.add("correct");
		}
		button.disabled = true;
	});
	nextButton.style.display = "block";		
}

function showScore(){
	resetState();
	questionElement.innerHTML = `Вы ответили правильно на ${score} из ${questions.length}!`;
	nextButton.innerHTML = "Повторить";
	nextButton.style.display = "block";
}

function handleNextButton(){
	currentQuestionIndex++;
	if(currentQuestionIndex < questions.length){
		showQuestion();
	}else{
		showScore();
	}
}
	nextButton.addEventListener("click", ()=>{
	if(currentQuestionIndex < questions.length){
		handleNextButton();
	}else{
		startQuiz();
	}
});

startQuiz();